
#include <BLEServer.h>
#include <Arduino.h>
// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#ifdef BLENETSOFT
    BLEServer *pServer;
    BLEService *pService;
    std::uint16_t iIdConexion = 0;
    std::string sValorCom1 = "";
    std::string sValorCom2 = "";
    bool deviceConnected = false;
    int giPositionChar1 = 0;
#else
    extern BLEServer *pServer;
    extern BLEService *pService;
    extern std::uint16_t iIdConexion;
    extern std::string sValorCom1;
    extern std::string sValorCom2;
    extern bool deviceConnected;
    extern int giPositionChar1;
#endif
//Servicio que englobara todas las characteristic
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"

//Characteristics de COM1
#define CHARACTERISTIC_UUID_COM1 "beb5483e-36e1-4688-b7f5-ea07361b26a8"

//Characteristics de COM2
#define CHARACTERISTIC_UUID_COM2 "16d39836-59e5-11ed-9b6a-0242ac120002"

//Characteristics usada como Bridge de comunicacion de internet
#define CHARACTERISTIC_UUID_RED "38f06f8e-59e5-11ed-9b6a-0242ac120002"

class BLENetSoft{
    public:
        static void init();
        static std::uint32_t getCountClientConnect();
};

class BLECom1:public Stream{
    public:
        virtual int read();
        void write(std::string);
        void writeWithNotify(std::string);
};

class BLECom2{
    public:
        std::string read();
        void write(std::string);
        void writeWithNotify(std::string);
};

class HttpBLE{
    public:
        std::string read();
        void write(std::string);
        std::string connect();
};
