#include <BLEDevice.h>
#ifdef BLENETSOFTCHAR
    BLECharacteristic *pCharacteristicCom1;
    BLECharacteristic *pCharacteristicCom2;
    BLECharacteristic *pCharacteristicRed;
#else
    extern BLECharacteristic *pCharacteristicCom1;
    extern BLECharacteristic *pCharacteristicCom2;
    extern BLECharacteristic *pCharacteristicRed;
#endif

class BLENETCharacteristic{
    public:
        static void init();
};