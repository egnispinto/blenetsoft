#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <Arduino.h>
#include <BLE2902.h>
#include "BLENetSoft.h"
#include "BLENETCharacteristic.h"

class Classbacks : public BLEServerCallbacks {
  void onConnect(BLEServer* pServer){
    if(deviceConnected)
      pServer->disconnect(pServer->getConnId());
    else{
      Serial.println("onConnect");
      iIdConexion = pServer->getConnId();
      deviceConnected = true;
    }
  }
  void onDisconnect(BLEServer* pServer) {
    Serial.println("onDisconnect");
 //   if(pServer->getConnectedCount() == 1){
    //  Serial.println("onDisconnect All");
      pServer->startAdvertising();
      deviceConnected = false;
   // }
 //   else
      
  }
};

  void BLENetSoft::init(){
    Serial.println("Starting BLE Server!");
    BLEDevice::init("ESP32-BLE-Server");
    pServer = BLEDevice::createServer();
    pService = pServer->createService(SERVICE_UUID);
    //Inicio las characteristics
    BLENETCharacteristic::init();
    //Resto de condfiguracion
    pCharacteristicCom2->setNotifyProperty(true);
    BLEDescriptor *pStatusDescriptor = new BLE2902();
    BLEDescriptor *pStatusDescriptor2 = new BLE2902();
    pCharacteristicCom2->addDescriptor(pStatusDescriptor); //Es necesario para que funcione la notificación en IONIC
    pCharacteristicRed->addDescriptor(pStatusDescriptor2); //Es necesario para que funcione la notificación en IONIC
    //pCharacteristicRed->setCallbacks(new RedCallback());
    pService->start();
    
    pServer->setCallbacks(new Classbacks()); //Para saber las conexiones y desconexiones

    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();
  }

  std::uint32_t BLENetSoft::getCountClientConnect(){
    return pServer->getConnectedCount();
  }


  /*
  CLASES PARA EMULAR CONEXIONES COM
  */
 
  int BLECom1::read(){
    std:int i = 0;
    char valueReturn = -1;
    /*while (true)
    {
      if(sValorCom1.length() > 0)
        break;
      delay(100);
      i++;
      if(i > 3)
        break;
    }*/
    if(sValorCom1.length() > 0 && giPositionChar1 < sValorCom1.length())
      valueReturn = sValorCom1[giPositionChar1];
    giPositionChar1++;
    return valueReturn;
  }

  void BLECom1::write(std::string sValueWrite){
    sValorCom1 = "";
    pCharacteristicCom1->setValue(sValueWrite); 
  }
  void BLECom1::writeWithNotify(std::string sValueWrite){
    sValorCom1 = "";
    pCharacteristicCom1->setValue(sValueWrite); 
    pCharacteristicCom1->notify(); 
  }


 // public:
  std::string BLECom2::read(){
    std:int i = 0;
    while (true)
    {
      if(sValorCom2.length() > 0)
        break;
      delay(100);
      i++;
      if(i > 3)
        break;
    }
      //std::istringstream iss{ sValorCom2 };
      return sValorCom2;  
  }
  void BLECom2::write(std::string sValueWrite){
    sValorCom2 = "";
    pCharacteristicCom2->setValue(sValueWrite); 
  }
  void BLECom2::writeWithNotify(std::string sValueWrite){
    sValorCom2 = "";
    pCharacteristicCom2->setValue(sValueWrite); 
    pCharacteristicCom2->notify(); 
  }

  std::string HttpBLE::read(){
    return pCharacteristicRed->getValue(); 
  }
  void HttpBLE::write(std::string sValueWrite){
    pCharacteristicRed->setValue(sValueWrite); 
  }
  std::string HttpBLE::connect(){
    pCharacteristicRed->setValue("Hola Red"); 
    pCharacteristicRed->notify();
    return this->read();
  }
