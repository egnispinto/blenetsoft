#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <Arduino.h>
#include <BLE2902.h>
#include "BLENetSoft.h" 
#define BLENETSOFTCHAR
#include "BLENETCharacteristic.h"

/*
Esta clase pertenece a el callbacks de las characteriistic definida para el COM1
*/
class Com1Callback: public BLECharacteristicCallbacks {
        void onWrite(BLECharacteristic *pCharacteristic) {
            std::string rxValue = pCharacteristic->getValue();
            std::uint32_t uCountClient = pServer->getConnectedCount();
            BLEAdvertisedDevice advertisedDevice;
            if (rxValue.length() > 0) {
                Serial.println(rxValue.c_str());
                sValorCom1 = rxValue;
            }
        }
        void onNotify(BLECharacteristic *pCharacteristic) {
            pCharacteristic->notify(true);
        }
        void onRead(BLECharacteristic* pCharacteristic){
            sValorCom1 = "";
            pCharacteristic->setValue("Soy el valor de lectura del COM1"); //Comentar esta linea luego de las pruebas
        }
};

/*
Esta clase pertenece a el callbacks de las characteriistic definida para el COM2
*/
class Com2Callback: public BLECharacteristicCallbacks {
        void onWrite(BLECharacteristic *pCharacteristic) {
        std::string rxValue = pCharacteristic->getValue();
        std::uint32_t uCountClient = pServer->getConnectedCount();
        if (rxValue.length() > 0) {
            Serial.println(rxValue.c_str());
            sValorCom2 = rxValue;
            giPositionChar1 = 0;
        }
        }
        void onNotify(BLECharacteristic *pCharacteristic) {
        }
        void onRead(BLECharacteristic* pCharacteristic){
        //pCharacteristic->setValue("Soy el valor de lectura del COM2");
        }
};

/*
Esta clase pertenece a el callbacks de las characteriistic definida para el canal de RED
*/
class RedCallback: public BLECharacteristicCallbacks {
        void onWrite(BLECharacteristic *pCharacteristic) {
        std::string rxValue = pCharacteristic->getValue();
        std::uint32_t uCountClient = pServer->getConnectedCount();
        if (rxValue.length() > 0) {
            Serial.print("Value changed... new value: ");
            Serial.println(rxValue.c_str());
        }
        }
        void onRead(BLECharacteristic* pCharacteristic){
        //Asignacion global
        sValorCom2 = pCharacteristic->getValue();
        }
};

void BLENETCharacteristic::init(){
    //SE INSTANCIAN LAS CLASES
    pCharacteristicCom1 = pService->createCharacteristic(
                                            CHARACTERISTIC_UUID_COM1,
                                            BLECharacteristic::PROPERTY_READ |
                                            BLECharacteristic::PROPERTY_WRITE
                                        );
    //Characteristics perteneciente al COM2
    pCharacteristicCom2 = pService->createCharacteristic(
                                            CHARACTERISTIC_UUID_COM2,
                                            BLECharacteristic::PROPERTY_READ |
                                            BLECharacteristic::PROPERTY_WRITE |
                                            BLECharacteristic::PROPERTY_NOTIFY|
                                            BLECharacteristic::PROPERTY_INDICATE
                                        );

    //Characteristics que se usara en la emulacion de RED
    pCharacteristicRed = pService->createCharacteristic(
                                            CHARACTERISTIC_UUID_RED,
                                            BLECharacteristic::PROPERTY_READ |
                                            BLECharacteristic::PROPERTY_WRITE |
                                            BLECharacteristic::PROPERTY_NOTIFY|
                                            BLECharacteristic::PROPERTY_INDICATE
                                        );
    pCharacteristicCom1->setCallbacks(new Com1Callback());
    pCharacteristicCom2->setCallbacks(new Com2Callback());
    pCharacteristicRed->setCallbacks(new RedCallback());
}

