#include <Arduino.h>
//En este orden
#define BLENETSOFT
#include "BLENetSoft.h" 


const char *valueSend = "";

//PARA PROBAR:
BLECom1 *pBLECom1;
Stream *psPrueba;
void setup()
{
  Serial.begin(9600);
  BLENetSoft::init();
  //BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);
  //BLEDevice::setSecurityCallbacks(new MySecurity());
  //BLESecurity *pSecurity = new BLESecurity();
  //pSecurity->setStaticPIN(123456789); 
  
  Serial.println("Characteristic defined! Now you can read it in the Client!");
}

void loop()
{
  std::uint32_t uCountClient = BLENetSoft::getCountClientConnect();
  if(deviceConnected){
    valueSend = "*** NOTIFY: (0)***\n";
    pBLECom1->writeWithNotify(valueSend);
    psPrueba = pBLECom1;
    Serial.println(psPrueba->read());
  }
  
  delay(1000);
}

